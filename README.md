# Language Helper

## Description
A simple C++ console application to help you finding language learning material!

## Project status
In development.

## Compatibility
Currently the app runs only on Linux.

## Installation
Clone the repository to your PC and launch the "language_helper.out".

## License
GNU General Public License v3.0.
