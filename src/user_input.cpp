#include "user_input.h"

UserInput::UserInput() : jlpt("None"), french_lvl("None"), eng_lvl("None") {
    std::cout << "Please enter for what language you would like to search info for (jp / eng / fr): ";
    std::cin >> user_lang;
}

std::string UserInput::jlpt_get() { return jlpt; }
std::string UserInput::eng_lvl_get() { return eng_lvl; }
std::string UserInput::french_lvl_get() { return french_lvl; }

void UserInput::jlpt_set(std::string jlpt) { this->jlpt = jlpt; }
void UserInput::eng_lvl_set(std::string eng_lvl) { this->eng_lvl = eng_lvl; }
void UserInput::french_lvl_set(std::string french_lvl) { this->french_lvl = french_lvl; }

void UserInput::language_set() {
    std::string user_input;
    UserInput language_check;
    if(user_lang == "jp") std::cout << "こんにちは! What's your Japanese level?\n"; std::cin >> user_input; language_check.jlpt_set(user_input); 
    if(user_lang == "eng") std::cout << "Hello! What's your English level?\n";std::cin >> user_input; language_check.eng_lvl_set(user_input);
    if(user_lang == "fr") std::cout << "Bonjour! What's your French level?\n"; std::cin >> user_input; language_check.french_lvl_set(user_input);
}