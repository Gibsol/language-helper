#ifndef FRENCH_H
#define FRENCH_H

#include <iostream>

class French {
private:
    std::string french_a1;
    std::string french_a2;
    std::string french_b1;
    std::string french_b2;
    std::string french_c1;
    std::string french_c2;
public:
    std::string french_a1_mat();
    std::string french_a2_mat();
    std::string french_b1_mat();
    std::string french_b2_mat();
    std::string french_c1_mat();
    std::string french_c2_mat();
};

#endif