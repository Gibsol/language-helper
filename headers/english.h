#ifndef ENGLISH_H
#define ENGLISH_H

#include <iostream>

class English {
private:
    std::string eng_a1;
    std::string eng_a2;
    std::string eng_b1;
    std::string eng_b2;
    std::string eng_c1;
    std::string eng_c2;
public:
    std::string eng_a1_mat();
    std::string eng_a2_mat();
    std::string eng_b1_mat();
    std::string eng_b2_mat();
    std::string eng_c1_mat();
    std::string eng_c2_mat();
};

#endif