#ifndef JAPANESE_H
#define JAPANESE_H

#include <iostream>

class Japanese {
private:
    std::string jlpt5;
    std::string jlpt4;
    std::string jlpt3;
    std::string jlpt2;
    std::string jlpt1;
public:
    std::string jlpt5_mat();
    std::string jlpt4_mat();
    std::string jlpt3_mat();
    std::string jlpt2_mat();
    std::string jlpt1_mat();
};

#endif