#ifndef USER_INPUT_H
#define USER_INPUT_H

#include <iostream>

class UserInput {
private:
    std::string jlpt;
    std::string eng_lvl;
    std::string french_lvl;
    std::string user_lang;
public:
    std::string jlpt_get();
    std::string eng_lvl_get();
    std::string french_lvl_get();

    void jlpt_set(std::string jlpt);
    void eng_lvl_set(std::string eng_lvl);
    void french_lvl_set(std::string french_lvl);

    void language_set(); 
};

#endif
